#ifndef RTUslave_h
#define RTUslave_h
#include "Arduino.h"
#include <SoftwareSerial.h>

struct CRC16{
    unsigned char CR0;
    unsigned char CR1;
};


class RTUslave {

    public:
    RTUslave();
    RTUslave(int address, SoftwareSerial& RTU_Serial);
    ~RTUslave();
    
    void init(int address, SoftwareSerial& RTU_Serial);
    void set_request(byte* buf, int size);
    void send_request();
    void check_result();
    void check_result_msg();
    bool set_address(int address);
    void set_address_msg();
    
    bool set_fn_get_data();
    void get_data_callback();
    void set_user_callback(void (*user_callback)(byte*));
           
    void print_HEX(byte* buf, int n);
  
    uint16_t CRC16_MODBUS(byte* buf, uint16_t size);
    CRC16 CRC16_MODBUS_CRS(byte* buf, uint16_t size);
    bool CRC16_check(byte* buf, uint16_t size);

  
    unsigned long m_request_interval  = 2000;                   // interval at which to send request (milliseconds)
    unsigned long m_check_interval    = m_request_interval/10;  // interval at which to send request (milliseconds)

    
    private:
    int m_address = 0;
    int m_new_address = 0;
    bool m_address_changed = false;
    //SoftwareSerial &m_RTU_Serial;
    SoftwareSerial *m_RTU_Serial;
    bool m_is_adress_set    = false;
    byte *m_request_buf       = nullptr;
    byte *m_result_buf        = nullptr;
    int m_request_size       = 0;
    int m_result_size        = 0;
    void (RTUslave::* m_check_callback_fn)() = &RTUslave::check_result_msg;
    void (*m_user_callback)(byte*) = NULL;
    
    unsigned long m_request_next_time = 0;                     // will store last time request was send
    unsigned long m_check_next_time = 0;                       // will store last time request was send

    const int m_slaves_id;
    static int slaves_count;
    static int active_slave;

};

#endif
