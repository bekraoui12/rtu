#include <SoftwareSerial.h>
#include "Arduino.h"
#include <RTUslave.h>


SoftwareSerial RTU_Serial(D5, D6); // RX | TX

#define WD_addr       1  //wind direction sensor address
#define WS_addr       2  //wind speed address

RTUslave WD_sensor(WD_addr, RTU_Serial); // RX | TX 
RTUslave WS_sensor(WS_addr, RTU_Serial); // RX | TX

void WS_callback(byte* result);
void WD_callback(byte* result);


void setup() {

pinMode(D5, INPUT);
pinMode(D6, OUTPUT);

  Serial.begin(115200);
  
  RTU_Serial.begin(9600);


  WD_sensor.set_user_callback(WD_callback);
  WD_sensor.set_fn_get_data();

  //WS_sensor.set_address(2);

  WS_sensor.set_user_callback(WS_callback);
  WS_sensor.set_fn_get_data();

}

void loop() {

  WD_sensor.send_request();
  WD_sensor.check_result();

  WS_sensor.send_request();
  WS_sensor.check_result();
}


void WD_callback(byte* result){
  Serial.println("WD:");
  int WD = result[4] | result[3] << 8;
  Serial.println(WD);
}

void WS_callback(byte* result){
  Serial.println("WS:");
  int WS_int = result[4] | result[3] << 8;
  float WS = (float)WS_int;
  WS = WS / 10.0 ;
  Serial.println(WS);
}
