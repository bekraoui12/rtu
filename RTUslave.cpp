#include"RTUslave.h"
#include <SoftwareSerial.h>



int RTUslave::slaves_count      = 0;
int RTUslave::active_slave      = 0;

RTUslave::RTUslave():m_slaves_id(slaves_count)
{
    slaves_count++;
}

RTUslave::RTUslave(int address, SoftwareSerial& RTU_Serial):m_RTU_Serial(&RTU_Serial),m_address(address), m_slaves_id(slaves_count)
{
    slaves_count++;
}

RTUslave::~RTUslave(){
  delete m_RTU_Serial;
  m_RTU_Serial = nullptr;
  delete m_request_buf;
  m_request_buf = nullptr;
  delete m_result_buf;
  m_result_buf = nullptr;
}



void RTUslave::init(int address, SoftwareSerial& RTU_Serial)
{
    m_RTU_Serial = &RTU_Serial;
    m_address = address;
}

void RTUslave::set_request(byte* buf, int size){

  delete m_request_buf;
  m_request_buf = nullptr;

  CRC16 crc = CRC16_MODBUS_CRS(buf, size);
  m_request_size = size+2;
  m_request_buf = new byte[m_request_size];
  for(int i=0; i<size; i++){
    m_request_buf[i] = buf[i];
  }
  m_request_buf[m_request_size-2] = crc.CR0;
  m_request_buf[m_request_size-1] = crc.CR1;
  
  //Serial.println("CRC0:"); Serial.print(crc.CR0, HEX);
  //Serial.println("CRC1:"); Serial.print(crc.CR1, HEX);  Serial.println();
}

void RTUslave::send_request(){
    if(m_slaves_id == RTUslave::active_slave) {
        if(m_request_size != 0){
          unsigned long currentMillis = millis();
          if (currentMillis >= m_request_next_time) {
              // save the next time for sending request.
              m_request_next_time = currentMillis + m_request_interval;
              m_RTU_Serial->write(m_request_buf, m_request_size);
              //Serial.println("request was send : "); print_HEX(m_request_buf, m_request_size); Serial.println();
          }
        }
    }
}

void RTUslave::check_result(){
    if(m_slaves_id == RTUslave::active_slave) {
        if(m_request_size != 0){
          unsigned long currentMillis = millis();
          if (currentMillis >= m_check_next_time) {
              // save the next time for checking.
              m_check_next_time = currentMillis + m_check_interval;

              //Serial.println("checking result ..."); Serial.println();
              if (m_RTU_Serial->available()) {
                
                delete m_result_buf;
                m_result_buf = nullptr;
                m_result_buf = new byte[m_result_size];
                byte tmp_result_buf[m_result_size];
                
                //Serial.println(" m_RTU_Serial available"); Serial.println();

                for (int i = 0; i < m_result_size; i++) {
                    tmp_result_buf[i] = m_RTU_Serial->read(); // Then: Get them.
                }
                int address = tmp_result_buf[0];
                bool CRC16_check = RTUslave::CRC16_check(tmp_result_buf, m_result_size);
                //Serial.println(" CRC check bool : "); Serial.println(CRC16_check);
                  
                
                if(!CRC16_check){  // resent request
                    if(m_request_size != 0){
                          m_RTU_Serial->write(m_request_buf, m_request_size);
                      }
                } else if(address == m_address){
                    for (int i = 0; i < m_result_size; i++) {
                        m_result_buf[i] = tmp_result_buf[i]; // Then: Get them.
                    }
                    (this->*m_check_callback_fn)();
                    //Serial.println(" readed value from : "); Serial.println(m_address);
                    //print_HEX(m_result_buf, m_result_size); Serial.println();
                    if (RTUslave::active_slave < slaves_count-1 ) {RTUslave::active_slave++;}
                    else {RTUslave::active_slave = 0;}
                }        
              } else {
              //Serial.println("..."); 
              }
          }
        }
    }
}
void RTUslave::check_result_msg(){  //default call back
            Serial.println(" readed value from : "); Serial.println(m_address); Serial.println();
            };
    
bool RTUslave::set_address(int address) {
  m_new_address = address;
  byte request_tmp[4] = { m_address, 0x10, 0x01, address};
  set_request(request_tmp, sizeof(request_tmp));

  m_result_size = 6;
  m_result_buf = new byte[m_result_size];
  m_check_callback_fn = &RTUslave::set_address_msg;
}

void RTUslave::set_address_msg(){
    if(m_result_buf[3] == 0x01){
        m_address = m_new_address;
        m_request_size = 0;
        Serial.println("Address changed to : "); Serial.println(m_address); Serial.println();
    } else {
        Serial.println("Failed to change address");
    }
    
};

bool RTUslave::set_fn_get_data() {
  byte request_tmp[6] = { m_address, 0x03, 0x00, 0x00, 0x00, 0x01};
  set_request(request_tmp, sizeof(request_tmp));

  m_result_size = 7;
  m_check_callback_fn = &RTUslave::get_data_callback;
}

void RTUslave::get_data_callback(){
    //if(m_result_buf[3] == 0x01){
        //Serial.println("Data was readed from: "); Serial.println(m_address); Serial.println();
        m_user_callback(m_result_buf);
        
        
    //} else {
    //    Serial.println("Failed to read data from: "); Serial.println(m_address); Serial.println();
    //}
};

void RTUslave::set_user_callback(void (*user_callback)(byte*)){
    m_user_callback = user_callback;
};

void RTUslave::print_HEX(byte* buf, int n) {
  Serial.println("HEX:");
  for (int i = 0; i < n; ++i) {
    Serial.print(i ? ", 0x" : "{0x");
    if (buf[i] < 0x10) Serial.print("0");
    Serial.print(buf[i], HEX);
  }
  Serial.println("}");
}

uint16_t RTUslave::CRC16_MODBUS(byte* buf, uint16_t size){

    const uint16_t crc_ibm_table[256] = {
      0x0000, 0xc0c1, 0xc181, 0x0140, 0xc301, 0x03c0, 0x0280, 0xc241,
      0xc601, 0x06c0, 0x0780, 0xc741, 0x0500, 0xc5c1, 0xc481, 0x0440,
      0xcc01, 0x0cc0, 0x0d80, 0xcd41, 0x0f00, 0xcfc1, 0xce81, 0x0e40,
      0x0a00, 0xcac1, 0xcb81, 0x0b40, 0xc901, 0x09c0, 0x0880, 0xc841,
      0xd801, 0x18c0, 0x1980, 0xd941, 0x1b00, 0xdbc1, 0xda81, 0x1a40,
      0x1e00, 0xdec1, 0xdf81, 0x1f40, 0xdd01, 0x1dc0, 0x1c80, 0xdc41,
      0x1400, 0xd4c1, 0xd581, 0x1540, 0xd701, 0x17c0, 0x1680, 0xd641,
      0xd201, 0x12c0, 0x1380, 0xd341, 0x1100, 0xd1c1, 0xd081, 0x1040,
      0xf001, 0x30c0, 0x3180, 0xf141, 0x3300, 0xf3c1, 0xf281, 0x3240,
      0x3600, 0xf6c1, 0xf781, 0x3740, 0xf501, 0x35c0, 0x3480, 0xf441,
      0x3c00, 0xfcc1, 0xfd81, 0x3d40, 0xff01, 0x3fc0, 0x3e80, 0xfe41,
      0xfa01, 0x3ac0, 0x3b80, 0xfb41, 0x3900, 0xf9c1, 0xf881, 0x3840,
      0x2800, 0xe8c1, 0xe981, 0x2940, 0xeb01, 0x2bc0, 0x2a80, 0xea41,
      0xee01, 0x2ec0, 0x2f80, 0xef41, 0x2d00, 0xedc1, 0xec81, 0x2c40,
      0xe401, 0x24c0, 0x2580, 0xe541, 0x2700, 0xe7c1, 0xe681, 0x2640,
      0x2200, 0xe2c1, 0xe381, 0x2340, 0xe101, 0x21c0, 0x2080, 0xe041,
      0xa001, 0x60c0, 0x6180, 0xa141, 0x6300, 0xa3c1, 0xa281, 0x6240,
      0x6600, 0xa6c1, 0xa781, 0x6740, 0xa501, 0x65c0, 0x6480, 0xa441,
      0x6c00, 0xacc1, 0xad81, 0x6d40, 0xaf01, 0x6fc0, 0x6e80, 0xae41,
      0xaa01, 0x6ac0, 0x6b80, 0xab41, 0x6900, 0xa9c1, 0xa881, 0x6840,
      0x7800, 0xb8c1, 0xb981, 0x7940, 0xbb01, 0x7bc0, 0x7a80, 0xba41,
      0xbe01, 0x7ec0, 0x7f80, 0xbf41, 0x7d00, 0xbdc1, 0xbc81, 0x7c40,
      0xb401, 0x74c0, 0x7580, 0xb541, 0x7700, 0xb7c1, 0xb681, 0x7640,
      0x7200, 0xb2c1, 0xb381, 0x7340, 0xb101, 0x71c0, 0x7080, 0xb041,
      0x5000, 0x90c1, 0x9181, 0x5140, 0x9301, 0x53c0, 0x5280, 0x9241,
      0x9601, 0x56c0, 0x5780, 0x9741, 0x5500, 0x95c1, 0x9481, 0x5440,
      0x9c01, 0x5cc0, 0x5d80, 0x9d41, 0x5f00, 0x9fc1, 0x9e81, 0x5e40,
      0x5a00, 0x9ac1, 0x9b81, 0x5b40, 0x9901, 0x59c0, 0x5880, 0x9841,
      0x8801, 0x48c0, 0x4980, 0x8941, 0x4b00, 0x8bc1, 0x8a81, 0x4a40,
      0x4e00, 0x8ec1, 0x8f81, 0x4f40, 0x8d01, 0x4dc0, 0x4c80, 0x8c41,
      0x4400, 0x84c1, 0x8581, 0x4540, 0x8701, 0x47c0, 0x4680, 0x8641,
      0x8201, 0x42c0, 0x4380, 0x8341, 0x4100, 0x81c1, 0x8081, 0x4040,
    };

    uint16_t crc = 0xFFFF;
    
    for(int i=0; i<size; i++){
        unsigned char nLookupIndex = (crc ^ buf[i]) & 0xFF;
        crc = (crc >> 8) ^ crc_ibm_table[nLookupIndex];
    }
    
    return crc;
}

CRC16 RTUslave::CRC16_MODBUS_CRS(byte* buf, uint16_t size){
    CRC16 crc;
    uint16_t tmp_crc= CRC16_MODBUS(buf, size);
    crc.CR0 = tmp_crc & 0xFF;
    crc.CR1 = tmp_crc >> 8;
    return crc;
}

bool RTUslave::CRC16_check(byte* buf, uint16_t size){  
    CRC16 crc = CRC16_MODBUS_CRS(buf, size-2);
    if( (crc.CR0 == buf[size-2]) && (crc.CR1 == buf[size-1]) ) {return true;}
    else {return false;}
    
}
